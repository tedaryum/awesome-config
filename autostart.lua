-- Standard awesome library
local spawn = require("awful.spawn")

-- Startup programs
os.execute("setxkbmap -model pc105 -layout tr &")
os.execute("~/.screenlayout/dual-laptop.sh &")
-- os.execute("~/.config/awesome/script-nitrogen.sh &")

-- Programs starting with spawn_with_shell
spawn("nm-applet")
spawn("blueman-tray")
spawn("synergy")
